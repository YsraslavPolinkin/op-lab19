#include <stdio.h>
#include <math.h>

int ob(int a)
{
	bool fl = 0;
	if (a < 0)
	{
		fl = 1;
		a = -a;
	}
	int b = 0, i = 1, aa = a;
	while (aa >= 1)
	{
		aa /= 10;
		i *= 10;
	}
	while (a >= 1)
	{
		i /= 10;
		b += (a % 10) * i;
		a /= 10;
	}
	if (fl) b = -b;
	return b;
}

int main()
{
	printf("Polinkin yaroslav\n");
	int a;
	printf("Enter number: "); scanf_s("%d", &a);
	printf("%d", ob(a));
	return 0;
}
